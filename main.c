﻿/*
 * Window Docker
 *
 * Copyright (C) 2011 Nikita Zlobin <cook60020tmp@mail.ru>
 *
 **************************************************************************
 * This file is part of Window Docker utility
 **************************************************************************
 *
 * Window Docker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Window Docker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Window Docker. If not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdint.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include <gdk/gdkx.h>

#include "main.h"
#include <stdlib.h>

static GError * error;

args_t args = {
  .side = SIDE_NONE,
  .sideStr = NULL,
  .pos = 0.0,
  .border = 1,

  .autohiding = FALSE,
  .show_delay = 0.1,
  .hide_delay = 0.1,

  .x = 0, .y = 0,

  .last = NULL
};

PanelWindow * panel;
struct list_head win_info = LIST_HEAD_INIT (win_info);
#if 1

static gboolean
on_plug_removed (GtkSocket * socket, gpointer data)
{ g_print ("Plug removed\n"); return TRUE; }

static
void fill_Panel (PanelWindow * panel)
{
  GtkWidget * win;
  GtkWidget * frame;
  GtkWidget * hbox;

  win = panelWindow_Container (panel);

  frame = gtk_frame_new (NULL);
  gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_OUT);
  gtk_container_add (GTK_CONTAINER (win), frame);

  switch (args.side)
    {
    case SIDE_NONE:
    case SIDE_TOP:
    case SIDE_BOTTOM:
    case SIDE_OTHER:
      hbox = gtk_hbox_new (FALSE, 0); break;
    case SIDE_LEFT:
    case SIDE_RIGHT:
      hbox = gtk_vbox_new (FALSE, 0); break;
    }
  gtk_container_add (GTK_CONTAINER (frame), hbox);

  panel->container = hbox;
}

static
gboolean add_window (PanelWindow * panel, GdkNativeWindow id)
{
  __label__ failed;
  g_print ("Grabbing window %lli\n", (long long)id);

  GdkWindow * win;
  win_info_t * win_info_new = malloc (sizeof (win_info_t));

  GtkWidget * socket;
  socket = gtk_socket_new ();
  g_signal_connect (G_OBJECT (socket), "plug-removed", G_CALLBACK (on_plug_removed), NULL);
  gtk_box_pack_start (GTK_BOX (panel->container), socket, FALSE, TRUE, 0);

  win = gdk_window_foreign_new (id);
  if (! win) {
    g_print ("ERROR: Can't get info about specified window\n");
    goto failed;
  }

  gdk_window_get_origin (win, & win_info_new->x, & win_info_new->y);
  gdk_drawable_get_size (GDK_DRAWABLE (win), & win_info_new->w, & win_info_new->h);
  g_object_unref (win);
 
  gtk_widget_set_size_request (socket, win_info_new->w, win_info_new->h);
  gtk_socket_add_id (GTK_SOCKET (socket), id);
  gtk_widget_show (socket);

  win = gtk_socket_get_plug_window (GTK_SOCKET (socket));
  if (! win) {
    g_print ("ERROR: Window was not embedded\n");
    goto failed;
  }

  win_info_new->id = gtk_socket_get_id (GTK_SOCKET (socket)),
  INIT_LIST_HEAD (& win_info_new->sibling);
  list_add_tail  (& win_info_new->sibling, & win_info);

  gdk_window_reparent (win, gtk_widget_get_window (socket), 0, 0);

  gdk_window_show (win);
  gdk_flush ();
  return TRUE;

 failed:
  free (win_info_new);
  return FALSE;
}

static void grab_WIDs (char ** args)
{
  __label__ useful_point, useless_point;

  if (!args) return;
  gboolean useless = TRUE;

  void * w_add_label = && useless_point;

  char ** arg = args;
  for (; * arg; arg++)
    {
      intmax_t wid = str_toIntMax (*arg);

      if (wid) goto * w_add_label;
      continue;

    useless_point:
      if (add_window (panel, wid))     {
	useless = FALSE;
	w_add_label = && useful_point; }
      continue;

    useful_point:
      add_window (panel, wid);
      continue;
    }
  if (useless) gtk_main_quit ();

  g_strfreev (args);
}

/* Callbacks */

static gboolean on_init (gpointer data)
{
  grab_WIDs (args.last);
  return FALSE;
}

void container_on_quit (GtkWidget * sock, gpointer data)
{
  GdkWindow * pluged_win;

  pluged_win = gtk_socket_get_plug_window (GTK_SOCKET (sock));
  if (! pluged_win) return;

  GdkNativeWindow  id = gtk_socket_get_id (GTK_SOCKET (sock));

  win_info_t * w_info = (win_info_t *)win_info.next;
  while (1)
    {
      if (& w_info->sibling == & win_info) return;
      if (w_info->id == id)   break;
      w_info = (win_info_t *)w_info->sibling.next;
    }
  GdkWindow * parent
    = gdk_screen_get_root_window (gtk_widget_get_screen (panel->win));

  gdk_window_reparent (pluged_win, parent, w_info->x, w_info->y);
  gdk_window_resize   (pluged_win, w_info->w, w_info->h);
  gdk_window_show (pluged_win);

  /* Compensate window auto-placing, used in most window managers */
  gdk_window_move (pluged_win, w_info->x, w_info->y);

  list_del (& w_info->sibling);
  free (w_info);
}

static gboolean on_quit (gpointer data)
{
  static gboolean used;
  if (used) return 0;

  gtk_container_foreach (GTK_CONTAINER (panel->container), container_on_quit, NULL);

  used = TRUE;
  return 0;
}
#endif

static
void init (PanelWindow * panel)
{
  fill_Panel (panel);

  if (args.side == SIDE_NONE) {
      g_print ("floating\n");
      panelWindow_Side_Set (panel, SIDE_NONE);
      gtk_window_move (GTK_WINDOW (panelWindow_Container (panel)), args.x, args.y);
  } else {
      g_print ("docked\n");
      panelWindow_Side_Set            (panel, args.side);

      panelWindow_AH_Toggle           (panel, args.autohiding);
      panelWindow_AH_ShowDuration_Set (panel, args.show_delay);
      panelWindow_AH_HideDuration_Set (panel, args.hide_delay);
      panelWindow_AH_Border_Set       (panel, args.border);
      panelWindow_Pos_Set             (panel, args.pos);
  }
  gtk_widget_show_all (panel->win);
}

/* Command line parsing */
/* {{{ */

static GOptionEntry options[] = {
  /*{ "config", 'c', 0, G_OPTION_ARG_FILENAME, & fileName,
    "Use other file to load and save configuration", NULL }, */

  { "x", 'x', 0, G_OPTION_ARG_INT, & args.x,
    "X coordinate in floating mode",
    "0" },

  { "y", 'y', 0, G_OPTION_ARG_INT, & args.y,
    "Y coordinate in floating mode",
    "0" },

  { "orientation", 'o', 0, G_OPTION_ARG_STRING, & args.sideStr,
    "Docked panel side.",
    "top, left, right, bottom" },

  { "position", 'p', 0, G_OPTION_ARG_DOUBLE, & args.pos,
    "Docked panel position.",
    "0.0...1.0" },

  { "border", 'b', 0, G_OPTION_ARG_INT, & args.border,
    "Size of visible border in hidden state",
    "0" },

  { "autohiding", 'a', 0, G_OPTION_ARG_NONE, & args.autohiding,
    "Automatically hide panel when mouse leaves it.",
    NULL},

  { "show-delay", 's', 0, G_OPTION_ARG_DOUBLE, & args.show_delay,
    "Show delay in seconds.",
    "0.1" },

  { "hide-delay", 'h', 0, G_OPTION_ARG_DOUBLE, & args.hide_delay,
    "Hide delay in seconds.",
    "0.1" },

  { G_OPTION_REMAINING, 0,  0, G_OPTION_ARG_STRING_ARRAY, & args.last,
    NULL, NULL },
NULL };

gboolean args_check (args_t * args)
{
  if (! args->last) return FALSE;

  if (args->sideStr) {
      __label__ done;

      if (strcmp (args->sideStr, "n") == 0 || strcmp (args->sideStr, "N") == 0 || strcmp (args->sideStr, "none") == 0)
	{ args->side = SIDE_NONE;  goto done; };

      if (strcmp (args->sideStr, "t") == 0 || strcmp (args->sideStr, "T") == 0 || strcmp (args->sideStr, "top") == 0)
      { args->side = SIDE_TOP;    goto done; };

      if (strcmp (args->sideStr, "b") == 0 || strcmp (args->sideStr, "B") == 0 || strcmp (args->sideStr, "bottom") == 0)
      { args->side = SIDE_BOTTOM; goto done; };

      if (strcmp (args->sideStr, "l") == 0 || strcmp (args->sideStr, "L") == 0 || strcmp (args->sideStr, "left") == 0)
      { args->side = SIDE_LEFT;   goto done; };

      if (strcmp (args->sideStr, "r") == 0 || strcmp (args->sideStr, "R") == 0 || strcmp (args->sideStr, "right") == 0)
      { args->side = SIDE_RIGHT;  goto done; };

      args->side = SIDE_NONE;
      done:;
  } else args->side = SIDE_NONE;

  return TRUE;
}

/* Command line parsing */
/* }}} */

/****************[   M A I N   ]********************/

int main (int argc, char ** argv)
{
  if (! gtk_init_with_args (&argc, &argv, "WID", options, NULL, & error)) {
    g_print ("Could not initialize GTK: %s\n", error->message);
    goto shutdown_0; }

  if (! args_check (& args)) goto shutdown_0;

  panel = panelWindow_Alloc ();
  panelWindow_Init (panel);

  /* TODO: Configuration storage */

  init (panel);

  g_signal_connect (G_OBJECT (panel->win), "delete-event", G_CALLBACK (gtk_main_quit),   NULL);

  gtk_init_add (   on_init, NULL);
  gtk_quit_add (0, on_quit, NULL);

  sig_handler_init ();

  gtk_main ();

  shutdown_0: return 0;
}
