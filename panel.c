/*
 * Window Docker
 *
 * Copyright (C) 2011 Nikita Zlobin <cook60020tmp@mail.ru>
 *
 **************************************************************************
 * This file contains desktop panel implementation, used in Window Docker
 **************************************************************************
 *
 * Window Docker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Window Docker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Window Docker. If not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "panel.h"
#include <stdlib.h>

static PanelWindow defaults = {
  .win = NULL,
  .container = NULL,
  .orig = { .x = 0, .y = 0 },

  .size = 0,
  .pos = {
    .x = 0, .y = 0,
    .hidden = { .x = 0, .y = 0 }
  },

  .docking = {
    .side = SIDE_NONE,
    .pos = 0.0,
    .border = 1,
    .visible = TRUE,
    .timeout = { .show = 10, .hide = 10, .id = 0},
    .autohiding = FALSE
  },

  .enter_cb_id = 0,
  .leave_cb_id = 0
};

#include "panel-int-mod.h"

/**
 * Public API
 */

PanelWindow * panelWindow_Alloc (void)
{ return malloc (sizeof (PanelWindow)); }

void panelWindow_Init (PanelWindow * panel)
{
  *panel = defaults;

  GdkScreen * screen;
  screen = gdk_screen_get_default ();
  panel->scr.h = gdk_screen_get_height (screen);
  panel->scr.w = gdk_screen_get_width (screen);

  panel->win = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_window_set_type_hint (GTK_WINDOW (panel->win), GDK_WINDOW_TYPE_HINT_DOCK);
  gtk_window_set_decorated (GTK_WINDOW (panel->win), FALSE);

  gtk_widget_set_events (panel->win, GDK_ALL_EVENTS_MASK);
  gtk_window_set_accept_focus (GTK_WINDOW (panel->win), TRUE);
  gtk_widget_set_can_focus (panel->win, TRUE);

  set_Gravity (panel);
  gtk_window_set_position (GTK_WINDOW (panel->win), GTK_WIN_POS_NONE);

  /* Auto-hiding */
  g_signal_connect (G_OBJECT (panel->win), "size-allocate", G_CALLBACK (on_resize),       panel);
  g_signal_connect (G_OBJECT (panel->win), "size-request",  G_CALLBACK (on_size_request), panel);

  panel->enter_cb_id = g_signal_connect (G_OBJECT (panel->win), "enter-notify-event", G_CALLBACK (on_enter), panel);
  panel->leave_cb_id = g_signal_connect (G_OBJECT (panel->win), "leave-notify-event", G_CALLBACK (on_leave), panel);
}

void panelWindow_Side_Set (PanelWindow * panel, side_t side)
{
  panel->docking.side = side;
  update_pos (panel);
  update_window_pos (panel);
}

void panelWindow_Pos_Set (PanelWindow * panel, double pos)
{
  panel->docking.pos  = pos;
  update_pos (panel);
  update_window_pos (panel);
}

void panelWindow_AH_Toggle (PanelWindow * panel, gboolean enabled)
{
  if (panel->docking.autohiding == enabled) return;
  panel->docking.autohiding = enabled;

  if (enabled) hide (panel);
  else         show (panel);
}

void panelWindow_AH_ShowDuration_Set (PanelWindow * panel, double sec)
{ panel->docking.timeout.show = 1 + sec * 1000; }

void panelWindow_AH_HideDuration_Set (PanelWindow * panel, double sec)
{ panel->docking.timeout.hide = 1 + sec * 1000; }

void panelWindow_AH_Border_Set (PanelWindow * panel, guint thickness)
{
  panel->docking.border = thickness;
  update_pos (panel);
  if (panel->docking.visible)
    update_window_pos (panel);
}
