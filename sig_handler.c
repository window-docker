/*
 * Window Docker
 *
 * Copyright (C) 2011 Nikita Zlobin <cook60020tmp@mail.ru>
 *
 **************************************************************************
 * This file is part of Window Docker utility
 **************************************************************************
 *
 * Window Docker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Window Docker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Window Docker. If not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "main.h"

#include <signal.h>
#include <string.h>

void on_terminate (int sig)
{
  g_print ("Quit...\n");
  gtk_main_quit ();
}

void sig_handler_init (void)
{
  struct sigaction act;
  memset (&act, '\0', sizeof (act));
  act.sa_handler = on_terminate;
  sigaction (SIGTERM, & act, 0);
  sigaction (SIGINT,  & act, 0);
  sigaction (SIGQUIT, & act, 0);
}
