#include <inttypes.h>

intmax_t str_toIntMax (char * str)
{
  int base;
  if (*str == '0') {
    switch (* ++str)
    {
      case '\0': return 0;  str++; break ;
      case 'x':
      case 'X': base = 16; str++; break;
      case 'b':
      case 'B': base = 2;  str++; break;
      default: base = 8; break;
    }
  } else base = 10;

  return strtoimax (str, 0, base);
}
