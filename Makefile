includes = -I./headers/3rd-party

release_flags = -O3
debug_flags = -O0 -ggdb3 -g3

gtk_flags = `pkg-config --cflags --libs gtk+-2.0`
sources = main.c panel.c misc.c sig_handler.c

all:
	gcc $(release_flags) -o window-docker $(sources) $(gtk_flags) $(includes)

debug:
	gcc $(debug_flags) -o window-docker $(sources) $(gtk_flags) $(includes)
