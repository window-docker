/*
 * Window Docker
 *
 * Copyright (C) 2011 Nikita Zlobin <cook60020tmp@mail.ru>
 *
 **************************************************************************
 * This file is part of Window Docker utility
 **************************************************************************
 *
 * Window Docker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Window Docker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Window Docker. If not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/** WARNING: This file is intended to be part of panel module only ;) */

#include <X11/Xlib.h>
#include <gdk/gdkx.h>

/**
 * Internal helpers
 */
/* {{{ */

static void panel_window_focus (PanelWindow * panel);
static void update_window_pos  (PanelWindow * panel);
static int  fix_border (PanelWindow * panel);
static void update_pos (PanelWindow * panel);
static gboolean show    (PanelWindow * panel);
static gboolean hide    (PanelWindow * panel);
static gboolean present (PanelWindow * panel);
static void set_Gravity (PanelWindow * panel);

static
void panel_window_focus (PanelWindow * panel)
{
#ifdef GDK_WINDOWING_X11
  XClientMessageEvent event;

  event.type = ClientMessage;
  event.window = GDK_WINDOW_XID (GTK_WIDGET (panel->win)->window);
  event.message_type = gdk_x11_get_xatom_by_name ("_NET_ACTIVE_WINDOW");
  event.format = 32;
  event.data.l[0] = 0;

  gdk_error_trap_push ();

  XSendEvent (GDK_DISPLAY (), GDK_ROOT_WINDOW (), False,
              StructureNotifyMask, (XEvent *) &event);

  gdk_flush ();

  if (gdk_error_trap_pop () != 0)
    g_critical ("Failed to focus panel window");
#endif

  gtk_window_present (GTK_WINDOW (panel->win));
}

static
void update_window_pos (PanelWindow * panel)
{
  if (panel->docking.visible)
       gtk_window_move (GTK_WINDOW (panel->win), panel->pos.x, panel->pos.y);
  else gtk_window_move (GTK_WINDOW (panel->win), panel->pos.hidden.x, panel->pos.hidden.y);
}

static
int fix_border (PanelWindow * panel)
{
  switch (panel->docking.side) {
    case SIDE_LEFT:  return (panel->docking.border < panel->size.w) ? panel->docking.border : panel->size.w;
    case SIDE_RIGHT: return (panel->docking.border < panel->size.w) ? panel->docking.border : panel->size.w;
    case SIDE_TOP:    return (panel->docking.border < panel->size.h) ? panel->docking.border : panel->size.h;
    case SIDE_BOTTOM: return (panel->docking.border < panel->size.h) ? panel->docking.border : panel->size.h;
  }
}

static inline
int docked_pos_to_pixels (int range, int len, double pos)
{ return (range < len) ? 0 : (range - len) * pos; }

static
void update_pos (PanelWindow * panel)
{
  g_print ("update_pos: ");
  switch (panel->docking.side) {
    case SIDE_NONE:
        g_print ("side = NONE, ");
	break;

    case SIDE_LEFT:
        panel->pos.x = 0;
        panel->pos.y = docked_pos_to_pixels (panel->scr.h, panel->size.h, panel->docking.pos);
        panel->pos.hidden.x = fix_border(panel) - panel->size.w;
        panel->pos.hidden.y = panel->pos.y;
	g_print ("side = LEFT, ");
        break;

    case SIDE_RIGHT:
        panel->pos.x = panel->scr.w - panel->size.w;
        panel->pos.y = docked_pos_to_pixels (panel->scr.h, panel->size.h, panel->docking.pos);
        panel->pos.hidden.x = panel->scr.w - fix_border(panel);
        panel->pos.hidden.y = panel->pos.y;
	g_print ("side = RIGHT, ");
        break;

    case SIDE_TOP:
        panel->pos.y = 0;
        panel->pos.x = docked_pos_to_pixels (panel->scr.w, panel->size.w, panel->docking.pos);
        panel->pos.hidden.y = fix_border(panel) - panel->size.h;
        panel->pos.hidden.x = panel->pos.x;
	g_print ("side = TOP, ");
        break;

    case SIDE_BOTTOM:
        panel->pos.y = panel->scr.h - panel->size.h;
        panel->pos.x = docked_pos_to_pixels (panel->scr.w, panel->size.w, panel->docking.pos);
        panel->pos.hidden.y = panel->scr.h - fix_border(panel);
        panel->pos.hidden.x = panel->pos.x;
	g_print ("side = BOTTOM, ");
        break;
      
  default: g_print ("Unknown side, ");
  }
  g_print ("scr.W = %i, scr.H = %i, size = %i, pos = %f, X = %i, Y = %i, hidden_X = %i, hidden_Y = %i, border = %i\n",
	   panel->scr.w, panel->scr.h,
	   panel->size.w, panel->docking.pos,
	   panel->pos.x, panel->pos.y,
	   panel->pos.hidden.x, panel->pos.hidden.y,
	   fix_border (panel));
}

static
gboolean show (PanelWindow * panel)
{
  panel->docking.visible = TRUE;
  update_window_pos (panel);
  return FALSE;
}

static
gboolean hide (PanelWindow * panel)
{
  panel->docking.visible = FALSE;
  update_window_pos (panel);
  return FALSE;
}

static
gboolean present (PanelWindow * panel)
{
  show (panel);
  panel_window_focus (panel);
  if (! gtk_window_is_active (GTK_WINDOW (panel->win)) )
      g_print ("Failed to get focus\n");
  return FALSE;
}

static
void set_Gravity (PanelWindow * panel)
{
  switch (panel->docking.side)
  {
    case SIDE_NONE:   break;
    case SIDE_LEFT:   gtk_window_set_gravity (GTK_WINDOW (panel->win), GDK_GRAVITY_WEST); break;
    case SIDE_RIGHT:  gtk_window_set_gravity (GTK_WINDOW (panel->win), GDK_GRAVITY_EAST); break;
    case SIDE_TOP:    gtk_window_set_gravity (GTK_WINDOW (panel->win), GDK_GRAVITY_NORTH); break;
    case SIDE_BOTTOM: gtk_window_set_gravity (GTK_WINDOW (panel->win), GDK_GRAVITY_SOUTH); break;
  };
}

/* Internal helpers */
/* }}} */

/**
 * Callbacks
 */
/* {{{ */

static
void on_resize (GtkWidget * w, GtkAllocation * size, gpointer data)
{
  if (PANEL_WINDOW (data)->size.request) {
    PANEL_WINDOW (data)->size.request = FALSE;
    g_signal_handler_unblock (G_OBJECT (PANEL_WINDOW (data)->win), PANEL_WINDOW (data)->enter_cb_id);
    g_signal_handler_unblock (G_OBJECT (PANEL_WINDOW (data)->win), PANEL_WINDOW (data)->leave_cb_id);
  }

  PANEL_WINDOW (data)->size.w  = size->width;
  PANEL_WINDOW (data)->size.h = size->height;

  if (PANEL_WINDOW (data)->docking.side != SIDE_NONE)
    {
      update_pos (PANEL_WINDOW (data));
      update_window_pos (PANEL_WINDOW (data));
    }
}

static
void on_size_request (GtkWidget * w, GtkRequisition * size, gpointer data)
{
  if (! PANEL_WINDOW (data)->size.request) {
    PANEL_WINDOW (data)->size.request = TRUE;
    g_signal_handler_block (G_OBJECT (PANEL_WINDOW (data)->win), PANEL_WINDOW (data)->enter_cb_id);
    g_signal_handler_block (G_OBJECT (PANEL_WINDOW (data)->win), PANEL_WINDOW (data)->leave_cb_id);
  }
}

/**
 * Auto-hiding
 */
/* {{{ */

static
void slide_canceled (gpointer data)
{ PANEL_WINDOW (data)->docking.timeout.id = 0; }

static
gboolean on_enter (GtkWidget * w, GdkEventCrossing * ev, gpointer data)
{
  //if (PANEL_WINDOW (data)->size.request) return FALSE;

  /* Filter dublicate crossing for the same direction */
  if (! PANEL_WINDOW (data)->docking.visible) {

    /* If panel is already in necessary state, but has to change from it,... */
    if (PANEL_WINDOW (data)->docking.timeout.id)
      /* just stop timer */
      g_source_remove (PANEL_WINDOW (data)->docking.timeout.id);

    else {
      gpointer func;
      if (PANEL_WINDOW (data)->docking.autohiding)
	    func = present;
      else  func = panel_window_focus, PANEL_WINDOW (data)->docking.visible = TRUE;

      PANEL_WINDOW (data)->docking.timeout.id = g_timeout_add_full (
        G_PRIORITY_DEFAULT,
        PANEL_WINDOW (data)->docking.timeout.show,
        (GSourceFunc) func, data, slide_canceled            );
    }
  }
}

static
gboolean on_leave (GtkWidget * w, GdkEventCrossing * ev, gpointer data)
{
  /* Prevent misoperation on internal crossments */
  if (ev->detail == GDK_NOTIFY_INFERIOR) return FALSE;

  /* And on mis-crossing, possible on resizing */
  //if (PANEL_WINDOW (data)->size.request) return FALSE;

  /* Like in 'on_enter_slide ()' */
  if (PANEL_WINDOW (data)->docking.visible) {
    if (PANEL_WINDOW (data)->docking.timeout.id)
      g_source_remove (PANEL_WINDOW (data)->docking.timeout.id);
    else {
      if (PANEL_WINDOW (data)->docking.autohiding)
	PANEL_WINDOW (data)->docking.timeout.id = g_timeout_add_full (
          G_PRIORITY_DEFAULT,
          PANEL_WINDOW (data)->docking.timeout.hide,
          (GSourceFunc) hide, data, slide_canceled            );
      else
	PANEL_WINDOW (data)->docking.visible = FALSE;
    }
  }
}

/* Auto-hiding */
/* }}} */

/* Callbacks   */
/* }}} */
