﻿/*
 * Window Docker
 *
 * Copyright (C) 2011 Nikita Zlobin <cook60020tmp@mail.ru>
 *
 **************************************************************************
 * This file contains desktop panel API, used in Window Docker
 **************************************************************************
 *
 * Window Docker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Window Docker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Window Docker. If not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PANEL_H__
#define __PANEL_H__

#include <gtk/gtk.h>

/* Types */

typedef
struct { int h, w; } scr_t;

typedef
enum {
    SIDE_NONE,

    SIDE_TOP,
    SIDE_RIGHT,
    SIDE_BOTTOM,
    SIDE_LEFT,

    /* TODO:
     * Non-standard orientation - tilted sides (e.g. in frames with non-square geometry) */
    SIDE_OTHER
} side_t;

typedef
struct {
    scr_t scr;

    GtkWidget * win;
    GtkWidget * container;

    struct {
        GdkWindow * parent;
        gint        x, y;
    } orig;

    struct {
        guint w, h;
        gboolean request;
    } size;

    struct {
        int x, y;
        struct {
            int x, y;
        } hidden;
    } pos;

    struct {
        side_t side;
        double pos;
        int    border;
        gboolean autohiding;
        struct {
            int show, hide;
 	    guint id;
        } timeout;
        gboolean visible;
    } docking;

    guint enter_cb_id, leave_cb_id;
} PanelWindow;

#define PANEL_WINDOW( w ) ((PanelWindow *) w)

/**
 * PanelWindowRequest - request form for panel initialization.
 */

/* Init staff */
extern PanelWindow * panelWindow_Alloc (void);
extern void panelWindow_Init (PanelWindow * panel);

/* Features */
extern void panelWindow_AH_Toggle (PanelWindow * panel, gboolean autohide);
static inline gboolean panelWindow_AH_Used (PanelWindow * panel) { return panel->docking.autohiding; }

/* Parameters */
extern void panelWindow_Side_Set (PanelWindow * panel, side_t side);
extern void panelWindow_Pos_Set  (PanelWindow * panel, double pos);
extern void panelWindow_AH_ShowDuration_Set (PanelWindow * panel, double sec);
extern void panelWindow_AH_HideDuration_Set (PanelWindow * panel, double sec);
extern void panelWindow_AH_Border_Set       (PanelWindow * panel, guint thickness);

static inline side_t panelWindow_Side            (PanelWindow * panel) { return panel->docking.side;         }
static inline double panelWindow_Pos             (PanelWindow * panel) { return panel->docking.pos;          }
static inline double   panelWindow_AH_ShowDuration (PanelWindow * panel) { return panel->docking.timeout.show; }
static inline double   panelWindow_AH_HideDuration (PanelWindow * panel) { return panel->docking.timeout.hide; }
static inline guint    panelWindow_AH_Border       (PanelWindow * panel) { return panel->docking.border;       }

/* Get internal objects */
static inline GtkWidget * panelWindow_Container (PanelWindow * panel) { return panel->win; }

#endif /*__PANEL_H__ */
