﻿/*
 * Window Docker
 *
 * Copyright (C) 2011 Nikita Zlobin <cook60020tmp@mail.ru>
 *
 **************************************************************************
 * This file is part of Window Docker utility
 **************************************************************************
 *
 * Window Docker is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Window Docker is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Window Docker. If not, see <http://www.gnu.org/licenses/>
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __MAIN_H__
#define __MAIN_H__

#include <gtk/gtk.h>
#include "panel.h"
#include <stdint.h>
#include <klist.h>

/* Types */

typedef
struct {
    char * sideStr;
    side_t side;
    double pos;
    int    border;

    gboolean autohiding;
    double show_delay;
    double hide_delay;

    int x, y;

    char ** last;
} args_t;

typedef
struct {
    struct list_head sibling;
    GdkNativeWindow id;
    int x, y, w, h;
} win_info_t;

extern void     sig_handler_init (void);
extern intmax_t str_toIntMax (char * str);

#endif /* __MAIN_H__ */
